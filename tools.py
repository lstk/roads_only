import Image
from PIL import ImageFilter
import numpy
import os
import math
# from random import randint
# import pwd
# import grp


delimiter = ' '

__author__ = 'anatoly + lstk'


def get_pixels(x, y, path, size=0, is_need_save=False, save_path='', grey=False, do_ravel=True):
    img = Image.open(path)
    if size == 0:
        (width, height) = img.size
    else:
        width = size
        height = width
    left = x
    top = y
    box = (left, top, left + width, top + height)

    if grey:
        img = img.convert('L')
    img = img.filter(ImageFilter.SHARPEN)
    area = img.crop(box)
    I = numpy.asarray(area) / 256.

    if do_ravel:
        I = I.ravel()

    if is_need_save:
        newName = os.path.basename(path).split('.')[0]
        join = os.path.join(save_path, newName + '_crop' + str(x) + '_' + str(y) + '.jpeg')
        # uid = pwd.getpwnam("lstk").pw_uid
        # gid = grp.getgrnam("lstk").gr_gid
        area.save(join, 'jpeg')
        # os.chown(join, uid, gid)



    return I


def get_pixels_string(pixels_array):

    row = str(''.join(str(row) + delimiter for row in pixels_array)).strip()

    return row

def resize_image(path, size, propotions_save = 0):
    basewidth = size
    im = Image.open(path)
    if propotions_save == 1:
        wpercent = (basewidth / float(im.size[0]))
        hsize = int((float(im.size[1]) * float(wpercent)))
        im = im.resize((basewidth, hsize), Image.ANTIALIAS)
    else:

        im = im.resize((basewidth, basewidth), Image.ANTIALIAS)

    newName = os.path.basename(path)
    outputFilePath = 'resized' + "/small" + newName
    im.save(outputFilePath, "JPEG")
    return outputFilePath

def get_half_image(path):
    img = Image.open(path)
    width = img.size[0]
    # print(width-100)
    height = img.size[1]
    # print(height-100)
    newHeight = ((height/2)-60)
    ultranew = int(10 * round (float(newHeight)/10))
    print ultranew
    img3 = img.crop(
        (
            0,
            ultranew,
            width,
            height
        )
    )
    filename = os.path.basename(path)
    # join = os.path.join(save_path, 'crop' + str(x) + '_' + str(y) + '.jpeg')
    outputFilePath = 'half' + "/small" + filename
    img3.save(outputFilePath)
    return outputFilePath