#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'lstk'
from PIL import Image
import os
import time
import sys
from PyQt4 import QtCore, QtGui, uic, Qt

import numpy
import time
from getRoad import tools, getModel



class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        uic.loadUi('diploma.ui', self)
        # self.pushButton.clicked.connect(self.openFile)
        self.pushButton_2.clicked.connect(self.recogniteButton)
        self.pushButton.clicked.connect(self.showDialog)


    def openFile (self):
        self.scene = QtGui.QGraphicsScene()
        image_path = self.lineEdit.text()
        myPixmap = QtGui.QPixmap(image_path)
        myScaledPixmap = myPixmap.scaled([20, 20], Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.scene.addPixmap(myScaledPixmap)

        # self.graphicsView.setScene(self.scene)
        self.graphicsView.fitInView(myScaledPixmap)
        self.graphicsView.show()


    # def fakeReco (self):
    #     time.sleep(40)
    #     self.scene2 = QtGui.QGraphicsScene()
    #     image_path2 = '/home/lstk/Dropbox/rec/698rec.jpg'
    #     self.scene2.addPixmap(QtGui.QPixmap(image_path2))
    #     self.graphicsView_2.setScene(self.scene2)
    #     stringUTF = 'Общая оценка дорожного покрытия: удовлетворительно'
    #     d = stringUTF.decode('utf-8')
    #     self.label_3.setText(d)


    def showDialog(self):
        filenames = QtGui.QFileDialog.getOpenFileNames(self, 'Open file', '/home/lstk/getRoad/getRoad')
        outputline = ''
        for i in range(0, len(filenames)):
            outputline += filenames[i] + ';'
        self.lineEdit.setText(outputline)
        self.scene = QtGui.QGraphicsScene()
        image_path = filenames[0]
        # self.scene.addPixmap(QtGui.QPixmap(image_path))
        myPixmap = QtGui.QPixmap(image_path)
        myScaledPixmap = myPixmap.scaled(700, 400, QtCore.Qt.KeepAspectRatio)
        self.scene.addPixmap(myScaledPixmap)

        # self.graphicsView.setScene(self.scene)
        # self.graphicsView.fitInView(self.scene)
        self.graphicsView.setScene(self.scene)


    def recogniteButton(self):
        model = getModel.get_cnn_model('getRoad.pkl') #ROAD MODEL SHAPE = 25
        allfiles = self.lineEdit.text()
        allfiles2 = str(allfiles)
        filenamesarray = allfiles2.split(';')
        filelist = list()


        for image_file in filenamesarray[:-1]:
            newfile = image_file
            if self.checkBox.isChecked():
                newfile = tools.get_half_image(newfile)
            if self.checkBox_2.isChecked():
                newfile = tools.resize_image(newfile, 500)
            outputfile=self.recognizeRoad(newfile, model, 30, 3)
            filelist.append(outputfile)
            print filelist
        holesList = list()
        model = getModel.get_cnn_model('holes.pkl') #holes MODEL SHAPE = 15
        for image_file in filelist:
            holesList.append(self.recognizeHoles(image_file, model, 15, 3))
        self.scene2 = QtGui.QGraphicsScene()
        image_path2 = holesList[0]
        myPixmap = QtGui.QPixmap(image_path2)
        myScaledPixmap = myPixmap.scaled(700, 400, QtCore.Qt.KeepAspectRatio)
        self.scene2.addPixmap(myScaledPixmap)
        self.graphicsView_2.setScene(self.scene2)






    def recognizeHoles(self,pic_path, trained_model, cnn_input_size, n_channels):
        print 'started recognizing of ' + pic_path
        start = time.time()
        stride = 5
        # sup_size = 1000
        # tools.resize_image(pic_path, sup_size)
        pixels = tools.get_pixels(0, 0, pic_path, do_ravel=False)
        result_pixels = numpy.zeros_like(pixels)
        print len(pixels)
        width = pixels.shape[0]
        height = pixels.shape[1]
        steps_num = width/stride
        for i in range(0, width, stride):
            for j in range(0, height, stride):
                pixels_square = pixels[i:i+cnn_input_size, j:j+cnn_input_size]
                if pixels_square.shape[0] != cnn_input_size or pixels_square.shape[1] != cnn_input_size:
                    break
                square = pixels_square.reshape(1, cnn_input_size, cnn_input_size, n_channels)
                y = trained_model(square)
                recognized_type = y.argmax()
                if recognized_type == 0:
                    # good_road = 0, bad_road = 1
                    result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = pixels_square * 256
                else :
                    x1 = numpy.array([256,0,0])
                    result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = numpy.multiply(pixels_square, x1)
            #else is bad road so we multiply red channel to maximum
            self.label_3.setText('step ' + str(i/stride) + ' of ' + str(steps_num))
        result_image = Image.fromarray(result_pixels.astype('uint8'))
        save_path = os.path.splitext(pic_path)[0] + 'reco_holes.jpg'
        if not os.path.exists(os.path.dirname(os.path.abspath(save_path))):
            os.makedirs(os.path.dirname(os.path.abspath(save_path)))
        result_image.save(save_path)
        end = time.time()
        print 'end recognizing, saved to ' + save_path
        print 'time elapsed: ' + str(end - start)
        return save_path

    def recognizeRoad(self, pic_path, trained_model, cnn_input_size, n_channels):
        print 'started recognizing of ' + pic_path
        start = time.time()
        stride = 10
        # sup_size = 1000
        # tools.resize_image(pic_path, sup_size)
        pixels = tools.get_pixels(0, 0, pic_path, do_ravel=False)
        result_pixels = numpy.zeros_like(pixels)
        width = pixels.shape[0]
        height = pixels.shape[1]
        steps_num = width/stride
        for i in range(0, width, stride):
            for j in range(0, height, stride):
                pixels_square = pixels[i:i+cnn_input_size, j:j+cnn_input_size]
                if pixels_square.shape[0] != cnn_input_size or pixels_square.shape[1] != cnn_input_size:
                    break
                square = pixels_square.reshape(1, cnn_input_size, cnn_input_size, n_channels)
                y = trained_model(square)
                recognized_type = y.argmax()
                if recognized_type == 1:
                    # road
                    result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = pixels_square * 256

            self.label_3.setText('step ' + str(i/stride) + ' of ' + str(steps_num))
        result_image = Image.fromarray(result_pixels.astype('uint8'))
        save_path = os.path.splitext('recognized' + pic_path)[0] + '.jpg'
        if not os.path.exists(os.path.dirname(os.path.abspath(save_path))):
            os.makedirs(os.path.dirname(os.path.abspath(save_path)))
        result_image.save(save_path)
        end = time.time()
        print 'end recognizing, saved to ' + save_path
        print 'time elapsed: ' + str(end - start)
        return save_path




app = QtGui.QApplication(sys.argv)
mw = MainWindow()
mw.show()
app.exec_()


# def recognize(pic_path, trained_model, cnn_input_size, n_channels):
#     print 'started recognizing of ' + pic_path
#     start = time.time()
#     stride = 2
#     # sup_size = 1000
#     # tools.resize_image(pic_path, sup_size)
#     pixels = tools.get_pixels(0, 0, pic_path, do_ravel=False)
#     result_pixels = numpy.zeros_like(pixels)
#     print len(pixels)
#     width = pixels.shape[0]
#     height = pixels.shape[1]
#     steps_num = width/stride
#     for i in range(0, width, stride):
#         for j in range(0, height, stride):
#             pixels_square = pixels[i:i+cnn_input_size, j:j+cnn_input_size]
#             if pixels_square.shape[0] != cnn_input_size or pixels_square.shape[1] != cnn_input_size:
#                 break
#             square = pixels_square.reshape(1, cnn_input_size, cnn_input_size, n_channels)
#             y = trained_model(square)
#             recognized_type = y.argmax()
#             if recognized_type == 0:
#                 # road
#                 result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = pixels_square * 256
#             else :
#                 x1 = numpy.array([256,0,0])
#                 result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = numpy.multiply(pixels_square, x1)
#         print 'step ' + str(i/stride) + ' of ' + str(steps_num)
#     result_image = Image.fromarray(result_pixels.astype('uint8'))
#     save_path = os.path.splitext('recognized' + pic_path)[0] + '.jpg'
#     if not os.path.exists(os.path.dirname(os.path.abspath(save_path))):
#         os.makedirs(os.path.dirname(os.path.abspath(save_path)))
#     result_image.save(save_path)
#     end = time.time()
#     print 'end recognizing, saved to ' + save_path
#     print 'time elapsed: ' + str(end - start)

# model = getModel.get_cnn_model('holes.pkl')
# os.path.join(root, file) - get full path
# #newfile = tools.get_half_image('/home/lstk/getRoad/getRoad/recognizedimg/roads_only/698.jpg', 'img')
# #newfile = tools.resize_image(newfile, 800, 1)
#tools.get_half_image('im/8thumb.JPG', 'im')
# #recognize ('/home/lstk/getRoad/getRoad/recognizedimg/roads_only/698.jpg', model, 15, 3)
# tools.get_pixels(0, 0, 'img/698.bmp', 100, True, 'img/crop/', grey=False)
# print ((os.listdir('img/roads_only/')))

# now = 0
# for root, dirs, files in os.walk('img/roads_only/'):
#     for file in files:
#         st = len(files)
#         if file.endswith('.bmp'):
#             recognize(os.path.join(root, file), model, 20, 3)
#             now += 1
#             print (now)
#             print ('iz')
#             print (st)
