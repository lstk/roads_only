__author__ = 'lstk'
import os
import random
import csv



from getRoad import tools



def getDatasetRows(cropPath, newFileName):

    #crop_size

    size = 15
    crop_save_path = cropPath
    source_csv_path = newFileName
    # should be like this to provide easy dataset copying

    if not os.path.exists(crop_save_path):
        os.makedirs(crop_save_path)

    source_csv = open(source_csv_path, 'rt')

    reader = csv.reader(source_csv, delimiter=',', quotechar='|')
    reader.next()  # because we have headers
    examples = []
    for row in reader:
        x = int(row[0])
        y = int(row[1])
        out = int(row[2])
        path = row[3]

        examples.append([x, y, out, path])

    random.shuffle(examples)

    rows = []
    answers = []
    for example in examples:
        x = example[0]
        y = example[1]
        out = example[2]
        image_path = example[3]
        pixels = tools.get_pixels(x, y, image_path, size, True, crop_save_path, grey=False)
        rows.append(pixels)
        out_vector = [0, 0]
        out_vector[out] = 1
        answers.append(out_vector)
    source_csv.close()
    return [rows, answers]
