__author__ = 'lstk'
from pylearn2.utils import serial
import theano

def get_cnn_model(path):
    model = serial.load(path)
    batch_size = 10
    model.set_batch_size(batch_size)
    X = model.get_input_space().make_theano_batch()
    Y = model.fprop(X)
    f = theano.function([X], Y)

    return f