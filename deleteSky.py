__author__ = 'lstk'
import os
import time

from getRoad import tools

workDir = "/home/lstk/getRoad/getRoad/img"
roadsDir = workDir+"/roads_only"
numberOfFiles = 0

start = time.clock()
# create directory for roads_only
if not os.path.exists(roadsDir):
    os.makedirs(roadsDir)
else:
    print("Directory already exists")

# edit all files in directory
for file in os.listdir(workDir):
    if file.endswith(".bmp"):
        tools.get_half_image(workDir + "/" + file, roadsDir)
        numberOfFiles+=1
        print(file)
        print(os.path.abspath(file))
print("End")
end = time.clock()
timer = end-start
print ("Ellapsed time: '%.2f'"%(timer))
print 'Files in work:', numberOfFiles