__author__ = 'lstk'
import time

from getRoad import getDatasetRows
import tools
import pwd
import grp
import os

print 'start making dataset'
start = time.time()

result_csv_path = 'holesData.csv'

result_csv = open(result_csv_path, "w")
[rows, answers] = getDatasetRows.getDatasetRows('img/crop/', 'holes.csv')
for i in range(0, len(rows)):
    row = rows[i]
    answer = answers[i]
    # answer[0] - because now the answer is the vector, for example [1 0]
    string = str(answer[0]) + tools.delimiter + tools.get_pixels_string(row)
    result_csv.write(string + "\n")
result_csv.close()
uid = pwd.getpwnam("lstk").pw_uid
gid = grp.getgrnam("lstk").gr_gid
os.chown('img/crop/', uid, gid)

end = time.time()
print 'end making dataset'
print 'time elapsed: ' + str(end - start)