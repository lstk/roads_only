__author__ = 'lstk'
import numpy
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix

from getRoad import getDatasetRows

delimiter = ' '


class PicDataset(DenseDesignMatrix):
    def __init__(self,
                 path='/home/getRoad/getRoad/holesData.csv',
                 axes=['b', 0, 1, 'c'],
                 one_hot=False,
                 with_labels=True,
                 start=None,
                 stop=None,
                 preprocessor=None,
                 fit_preprocessor=False,
                 fit_test_preprocessor=False):
        self.n_classes = 2
        self.img_shape = (3, 15, 15)
        self.img_size = numpy.prod(self.img_shape)
        self.args = locals()

        [X, y] = getDatasetRows.getDatasetRows('img/crop/', 'holes.csv')
        X = numpy.asarray(X)
        y = numpy.asarray(y)

        super(PicDataset, self).__init__(X=X, y=y)
